PKG_NAME := vcpipe-refdata

ifneq ($(shell git diff --name-only | wc -l),0)
GIT_STATE := dirty
else
GIT_STATE := clean
endif

BASE_IMAGE_NAME = registry.gitlab.com/dpipe/variantcalling/vcpipe-refdata
ifneq ($(IMAGE_TAG),)
IMAGE_TAG := $(IMAGE_TAG)
else
IMAGE_TAG := local
endif
IMAGE_NAME := $(BASE_IMAGE_NAME):$(IMAGE_TAG)

GIT_HASH := $(shell git rev-parse --short=8 HEAD)
GIT_TAG := $(shell git tag -l --points-at HEAD)
BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)
BASE_DIR := $(PWD)
DATA_DIR ?= $(BASE_DIR)/data
DOCKER_DATA_DIR := /opt/vcpipe-refdata/data
USER_ID ?= $(shell id -u)
GROUP_ID ?= $(shell id -g)

ifneq ($(DO_CREDS),)
DOCKER_OPTS += --env-file $(shell realpath $(DO_CREDS))
endif

# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# From: https://stackoverflow.com/questions/10858261/abort-makefile-if-variable-not-set
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2))))

define docker-template
docker run --rm -t \
	$(DOCKER_OPTS) \
	-v $(DATA_DIR):$(DOCKER_DATA_DIR) \
	$(IMAGE_NAME) \
	$(RUN_CMD)
endef

define run-template
@mkdir -p $(DATA_DIR)
$(eval RUN_CMD := vcpipe_refdata $(CMD_ARGS) $(ACTION) $(ACTION_ARGS))
$(docker-template)
endef

define chown-data
$(eval RUN_CMD := chown -R $(USER_ID):$(GROUP_ID) $(DOCKER_DATA_DIR))
$(docker-template)	
endef

.PHONY: test
test: ## run CI tests, not yet implemented
	$(eval RUN_CMD := echo TODO)
	$(docker-template)

.PHONY: build
build: ## builds docker container to run commands in 
	docker build -t $(IMAGE_NAME) .

push: ## pushes the docker container to the registry
	docker push $(IMAGE_NAME)

.PHONY: download-data
download-data: enforce-creds ## downloads all test data to DATA_DIR, skipping any already downloaded
	$(eval ACTION := download)
	$(run-template)
	$(chown-data)

.PHONY: upload-data
upload-data: enforce-creds ## uploads all new, packaged datasets in DATA_DIR to DigitalOcean
	$(eval ACTION := upload)
	$(run-template)

.PHONY: package-data
package-data: ## Marks all datasets in DATA_DIR as ready to upload
	$(eval ACTION := package)
	$(run-template)
	$(chown-data)

.PHONY: clean
clean: ## remove any intermediary or testing files
	-rm -rf build/ dist/ vcpipe_refdata.egg-info/
	find . -name '*.pyc' -delete

.PHONY: shell
shell: enforce-creds ## starts a bash shell in a new Docker container
	$(eval RUN_CMD := /bin/bash)
	$(eval DOCKER_OPTS += -i)
	$(docker-template)

.PHONY: enforce-creds
enforce-creds:
	@$(call check_defined, DO_CREDS, 'Use DO_CREDS to specify a file containing SPACES_KEY and SPACES_SECRET')

.PHONY: help
help:
	@echo
	@echo "All options require setting DO_CREDS to the path of a file containing SPACES_KEY/SPACES_SECRET"
	@echo "with your DigitalOcean Spaces credentials. Use bash format (e.g., SPACES_KEY=secret_key_here)"
	@echo
	@echo "The package-data and download-data steps will chown all the files in DATA_DIR to USER_ID:GROUP_ID"
	@echo "The default values are from your personal account ($(USER_ID):$(GROUP_ID)), but can be overridden."
	@echo "Setting GROUP_ID to a shared group is suggested if the data will be shared between multiple users."
	@echo
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

## local dev
.PHONY: check-env
check-env:
	@[ $(CONDA_DEFAULT_ENV) = $(PKG_NAME) ] || ( echo wrong conda env: $(CONDA_DEFAULT_ENV); exit 1 )

.PHONY: reset
reset: check-env clean
	pip uninstall vcpipe_refdata data_spaces -y
	python setup.py develop
