from setuptools import setup, find_packages

data_spaces_version = "a23a76e54c3391a6e0bbd4a8b9683dcb6d4c5303"

setup(
    name="vcpipe_refdata",
    version="0.1.0",
    py_modules=["vcpipe_refdata"],
    install_requires=["click==7.1.1", "data_spaces==0.1.2", "PyYAML==5.3.1"],
    dependency_links=[
        f"git+https://gitlab.com/ousamg/data-spaces@{data_spaces_version}#egg=data_spaces-0.1.2"
    ],
    entry_points={"console_scripts": ["vcpipe_refdata=vcpipe_refdata:cli",]},
)
