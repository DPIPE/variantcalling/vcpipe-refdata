FROM python:3.8.2-slim-buster
LABEL maintainer="OUS AMG <ella-support@medisin.uio.no>"
#ARG USER_ID
#ARG GROUP_ID

#RUN addgroup --gid $GROUP_ID gitlab-runner
#RUN adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID gitlab-runner

ENV DEBIAN_FRONTEND=noninteractive \
    LANGUAGE=C.UTF-8 \
    LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    DATA_DIR=/data

RUN apt-get update && apt-get install -y --no-install-recommends \
    bash \
    curl \
    file \
    git \
    jq \
    libcurl4 \
    make \
    mlocate \
    vim \
    wget && \
    pip install -U pip setuptools wheel

COPY setup.py vcpipe_refdata.py /opt/vcpipe-refdata/
WORKDIR /opt/vcpipe-refdata
RUN python setup.py install
COPY . /opt/vcpipe-refdata
# allow writing of logfile created by the python script:
RUN chmod a+w /opt/vcpipe-refdata
CMD [ "/bin/bash" ]