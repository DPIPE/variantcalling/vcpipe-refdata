#!/usr/bin/env python

import click
from collections import OrderedDict
from data_spaces import DataManager
import datetime
import json
from pathlib import Path
import logging
import os
import re
import sys
from yaml import load as load_yaml, dump as dump_yaml

# try to use libyaml, then fall back to pure python if not available
try:
    # we're using C/BaseLoader to ensure all values are strings as expected
    from yaml import CBaseLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import BaseLoader as Loader, Dumper

CLICK_SETTINGS = dict(help_option_names=["-h", "--help"])
VALID_NATURES = (
    "capturekit",
    "mutect2",
    "genomic",
    "regression",
    "funcAnnot",
    "mtdna"
)
ALL_TYPES = "all"
VALID_TYPES = (ALL_TYPES, "common", "index", "knownSites", "happy", "variantcalling", "bed", "chain")
MGR_OPTS = dict()
TOUCHFILE = "DATA_READY"
root_dir = Path(os.getcwd())
data_root = root_dir / "data"

# shared log config
logger = logging.getLogger(__name__)
log_template = "[%(asctime)s] %(name)-50s [%(levelname)-7s]  %(message)s"
log_format = logging.Formatter(log_template)
log_file = Path("refdata.log")


@click.group()
@click.pass_context
@click.option(
    "--spaces-key", envvar="SPACES_KEY", help="DigitalOCean Spaces API key",
)
@click.option(
    "--spaces-secret", envvar="SPACES_SECRET", help="DigitalOcean Spaces API secret",
)
@click.option("--verbose", "log_level", flag_value=logging.INFO, help="Increase log level")
@click.option("--debug", "log_level", flag_value=logging.DEBUG, help="Max logging")
@click.option(
    "--datasets",
    "-d",
    type=click.Path(exists=True, dir_okay=False),
    default="datasets.json",
    show_default=True,
    help="JSON file containing datasets and versions",
)
@click.option(
    "--threads",
    type=int,
    default=min(os.cpu_count(), 20),
    show_default=True,
    help="Maximum number of threads to use",
)
@click.option("--config-file", envvar="SPACES_CONFIG", default="/opt/vcpipe-refdata/spaces-config.json", type=click.Path(exists=True, dir_okay=False), help="Config file")
def cli(ctx, **kwargs):
    # init logger
    log_level = kwargs["log_level"] or logging.WARNING
    set_logs(log_level)

    # update DataManager opts
    MGR_OPTS["max_threads"] = kwargs["threads"]
    MGR_OPTS["access_key"] = kwargs["spaces_key"]
    MGR_OPTS["access_secret"] = kwargs["spaces_secret"]
    if kwargs["config_file"]:
        with open(kwargs["config_file"]) as config_file:
            MGR_OPTS.update(json.load(config_file))


    # load datasets info
    with open(kwargs["datasets"]) as datasets_file:
        datasets = json.load(datasets_file)

    # pass on shared data to all subcommands (download, upload, package)
    ctx.ensure_object(dict)
    ctx.obj["datasets"] = datasets


@cli.command(context_settings=CLICK_SETTINGS, help="Download specified data")
@click.pass_context
@click.option(
    "--data-type",
    multiple=True,
    type=click.Choice(VALID_TYPES),
    default=[ALL_TYPES,],
    show_default=True,
    help="Type of data to download",
)
@click.option(
    "--nature",
    multiple=True,
    type=click.Choice(VALID_NATURES),
    default=[ALL_TYPES, ],
    show_default=True,
    help="Only download data from a specific nature, can be repeated for multiple natures",
)
@click.option(
    "kind_ids",
    "--kind-id",
    multiple=True,
    metavar="KIND_ID",
    default=[],
    help="Only download data of a specific kind, can be repeated for multiple kinds",
)
@click.option(
    "version",
    "--version",
    multiple=False,
    metavar="VERSION",
    default=None,
    help="Only download a specific version. Default: the version in datasets.json",
)
def download(ctx, **kwargs):
    mgr = init_mgr()
    kwargs["ctx"] = ctx
    logger.info(
        f"Downloading nature(s) {kwargs['nature']}, kind(s) {kwargs['kind_ids']}, data type(s) {kwargs['data_type']}, version {kwargs['version']}"
    )
    datasets = get_datasets(**kwargs)

    for nature in datasets:
        for data_type in datasets[nature]:
            for kind, params in datasets[nature][data_type].items():
                if kwargs['version']:
                    params['version'] = kwargs['version']
                    print("will download  specified version " + params['version'])
                else:
                    print("will download version in datasets.json" + params['version'])
                mgr.download_package(**params)


@cli.command(
    context_settings=CLICK_SETTINGS, help="Upload specified data (must be packaged first)"
)
@click.pass_context
@click.option(
    "--nature",
    multiple=True,
    type=click.Choice(VALID_NATURES),
    default=[VALID_NATURES[0]],
    show_default=True,
    help="Only upload data from a specific nature, can be repeated for multiple natures",
)
@click.option(
    "kind_ids",
    "--kind-id",
    multiple=True,
    metavar="KIND_ID",
    default=[],
    help="Only upload data from a specific kind ID, can be repeated for multiple kinds",
)
def upload(ctx, **kwargs):
    mgr = init_mgr()
    kwargs["ctx"] = ctx
    kwargs["data_type"] = ALL_TYPES
    logger.info(f"Uploading nature(s) {kwargs['nature']}, kind(s) {kwargs['kind_ids']}")
    datasets = get_datasets(**kwargs)

    for nature in datasets:
        for data_type in datasets[nature]:
            for kind, params in datasets[nature][data_type].items():
                try:
                    mgr.upload_package(**params)
                except RuntimeError as e:
                    if "Found existing remote files" in str(e):
                        logger.warn(
                            f"Found existing files for {params['path']} version {params['version']} in DigitalOcean, skipping"
                        )
                    else:
                        raise


@cli.command(context_settings=CLICK_SETTINGS, help="Marks new data as ready for upload")
@click.pass_context
@click.option(
    "--nature",
    multiple=True,
    type=click.Choice(VALID_NATURES),
    default=[VALID_NATURES[0]],
    show_default=True,
    help="Only package data from a specific nature, can be repeated for multiple natures",
)
@click.option(
    "kind_ids",
    "--kind-id",
    multiple=True,
    metavar="KIND_ID",
    default=[],
    help="Only package data from a specific kind ID, can be repeated for multiple kinds",
)
def package(ctx, **kwargs):
    kwargs["ctx"] = ctx
    kwargs["data_type"] = VALID_TYPES[0]
    parsed_datasets = get_datasets(**kwargs)

    for nature in parsed_datasets:
        for data_type in parsed_datasets[nature]:
            for kind_id, params in parsed_datasets[nature][data_type].items():
                if not params["path"].exists():
                    logger.error(f"Cannot package {params['path']}: does not exist")
                    continue
                dataset_ready = params["path"] / TOUCHFILE
                dataset_name = f"{params['path']}"

                if dataset_ready.exists():
                    dataset_metadata = load_yaml(dataset_ready.open("rt"), Loader=Loader)
                    if dataset_metadata.get("version", "") == params["version"]:
                        logger.info(f"{dataset_name} already packaged, skipping")
                    else:
                        logger.error(
                            f"Found existing {dataset_name} version {dataset_metadata['version']}, but"
                            f" trying to generate version {params['version']}. Please delete "
                            f"{params['path'].relative_to(root_dir)} and try again."
                        )
                    continue

                dump_yaml(
                    {"version": params["version"], "timestamp": datetime.datetime.utcnow()},
                    dataset_ready.open("wt"),
                    Dumper=Dumper,
                )
                logger.info(f"Marked {dataset_name} as ready to upload")


@cli.command(
    context_settings=CLICK_SETTINGS, help="List kinds available for each selected nature"
)
@click.pass_context
@click.option(
    "natures",
    "--nature",
    multiple=True,
    type=click.Choice(VALID_NATURES),
    default=VALID_NATURES,
    show_default=True,
    help="Only list kinds of specific natures, can be repeated for multiple natures",
)
@click.option(
    "show_data_types",
    "--show-data-types",
    is_flag=True,
    show_default=True,
    help="List data types available for each kind",
)
def list_kinds(ctx, natures, show_data_types):
    kinds = {p: {} for p in natures}
    for nature in ctx.obj["datasets"]:
        if nature not in natures:
            continue

        for kind_id, kind in ctx.obj["datasets"][nature]["kinds"].items():
            kinds[nature][kind_id] = kind.get("data_types", [])

    for nature_name, nature_kinds in kinds.items():
        if nature_kinds:
            print(f"{nature_name}:")
            for kind_id in sorted(nature_kinds):
                kind_str = f"\t{kind_id}"
                if show_data_types:
                    kind_str += ": " + ", ".join(nature_kinds[kind_id])
                print(kind_str)
        else:
            print(f"{nature_name}: no kinds found")
        print()


def init_mgr():
    return DataManager(**MGR_OPTS)


def set_logs(log_level):
    logger.setLevel(log_level)
    file_handler = logging.FileHandler(log_file, encoding="utf-8")
    file_handler.setLevel(log_level)
    file_handler.setFormatter(log_format)
    logger.addHandler(file_handler)


def get_datasets(ctx, data_type, nature, kind_ids, version=None):
    list_natures = nature # input is singular to look better in terminal (--nature .., rather than --natures ..)
    final_opts = dict()
    _ = version # not used
    for dataset_nature_type in list_natures:
        # only two data_types, so only need a value instead of list
        if VALID_TYPES[0] in data_type:
            final_opts[dataset_nature_type] = {dt: {} for dt in VALID_TYPES[1:]}
        else:
            final_opts[dataset_nature_type] = {dt: {} for dt in data_type}

        # shortened variable for convenience
        kinds = ctx.obj["datasets"][dataset_nature_type]["kinds"]
        kind_pattern = "|".join(kind_ids)
        download_kinds = [x for x in kinds if re.search(kind_pattern, x)]

        # if no kinds in nature.data_type meet requirements, skip it
        if len(download_kinds) == 0:
            continue

        # otherwise build out version, path, and name params for each kind that meets criteria
        for kind_id in download_kinds:
            for dt in final_opts[dataset_nature_type]:
                if dt not in kinds[kind_id]["data_types"]:
                    logger.debug(f"Skipping {dataset_nature_type}/{kind_id}: no {dt} data")
                    continue
                data_dir = data_root / dataset_nature_type / dt / kind_id
                final_opts[dataset_nature_type][dt][kind_id] = {
                    "version": kinds[kind_id]["version"],
                    "path": data_dir.relative_to(root_dir),
                    "name": f"{dataset_nature_type}/{dt}/{kind_id}",
                }

    return final_opts
